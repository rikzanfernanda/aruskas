import Routes from "./config/Routes";
import "./assets/styles/core.scss";

function App() {
  return (
      <Routes/>
  );
}

export default App;
