import { Heading } from '@chakra-ui/react';
import React from 'react';

const HeadingAuth = ({title}) => {
    return <>
        <Heading size={['md', 'lg']} mb={[2]}>{title}</Heading>
    </>
}

export default HeadingAuth;