import { Box, Spinner, StackDivider, VStack } from '@chakra-ui/react';
import React, { useCallback, useEffect, useState } from 'react';
import { getHistories } from '../api/History';
import AlertFetch from './AlertFetch';
import HistoryListItem from './HistoryListItem';

const HistoryList = ({ limit }) => {
    const [isLoading, setIsLoading] = useState(true);
    const [data, setData] = useState([]);
    const [alert, setAlert] = useState({
        status: false,
        type: 'warning',
        message: '',
    });

    const fetchData = useCallback(async () => {
        try {
            const { data } = await getHistories(null, limit);
            if (data.status === 200) {
                setData(data.result);
            } else {
                setAlert({
                    status: true,
                    type: 'warning',
                    message: 'Ada kesalahan request data',
                });
            }
        } catch (error) {
            setAlert({
                status: true,
                type: 'error',
                message: error.message,
            });
        }
        setIsLoading(false);
    }, [limit]);

    useEffect(() => {
        fetchData();
    }, [fetchData])

    return <>
        {
            alert.status ? <AlertFetch alert={alert} /> :
                isLoading ?
                    <Box textAlign={'center'}>
                        <Spinner
                            thickness='4px'
                            speed='0.65s'
                            emptyColor='gray.200'
                            color='blue.400'
                            size='xl'
                        />
                    </Box>
                    :
                    <VStack align={'stretch'} spacing={[2, 4]} divider={<StackDivider my={0} borderColor='gray.200' />}>
                        {
                            data.map((item, index) => {
                                return <HistoryListItem key={index} item={item} />
                            })
                        }
                    </VStack>
        }
    </>
}

export default HistoryList;