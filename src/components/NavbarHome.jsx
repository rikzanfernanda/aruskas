import React from "react";
import Logo from "../assets/images/logo.png";
import {APP_NAME} from "../config/Config";
import {Link} from "react-router-dom";
import Cookies from "js-cookie";

const NavbarHome = ({active, handleActive}) => {
    return <>
        <nav className='navbar'>
            <div className='brand'>
                <div className='logo'>
                    <img src={Logo} alt='logo'/>
                    <span>{APP_NAME}</span>
                </div>
                <Link to='/' className='icon' onClick={handleActive}>
                    <i className="fa fa-bars"/>
                </Link>
            </div>
            {
                active &&
                <ul className='menu'>
                    {
                        Cookies.get('_TOKEN') ?
                            <li>
                                <Link to='/dashboard'>Dashboard</Link>
                            </li> :
                            <li>
                                <Link to='/login'>Login</Link>
                            </li>
                    }
                    <li>
                        <Link to='/register'>Register</Link>
                    </li>
                </ul>
            }
        </nav>
    </>
}

export default NavbarHome;