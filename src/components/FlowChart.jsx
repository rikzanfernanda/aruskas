import React, { useEffect, useState } from 'react';
import ReactApexChart from 'react-apexcharts';
import { getHistories } from '../api/History';

const FlowChart = () => {
    let init_flowChart = {
        series: [
            {
                name: 'Pemasukan',
                data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            },
            {
                name: 'Pengeluaran',
                data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            }
        ],
        options: {
            chart: {
                type: 'bar'
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '55%',
                    endingShape: 'rounded'
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            xaxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'],
            },
            yaxis: {
                title: {
                    text: 'Rp (Rupiah)'
                }
            },
            fill: {
                opacity: 1
            },
            tooltip: {
                y: {
                    formatter: function (val) {
                        return "Rp. " + val
                    }
                }
            }
        }
    }
    const [flowChart, setFlowChart] = useState(init_flowChart);

    useEffect(() => {
        fetchHistoryYear(new Date().getFullYear());
    }, []);

    const fetchHistoryYear = async (year) => {
        try {
            let { data } = await getHistories(year);
            console.log(data);
        } catch (error) {
            console.log(error.message);
        }
    }

    return <>
        <ReactApexChart
            options={flowChart.options}
            series={flowChart.series}
            type="bar"
            width="100%"
        />
    </>
}

export default FlowChart;