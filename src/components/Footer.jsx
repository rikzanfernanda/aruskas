import React from "react";

const Footer = () => {
    return <>
        <div className='footer'>
            A project by <a href="https://www.linkedin.com/in/rikzan-fernanda-9097831ab/" target='_blank' rel="noreferrer">Rikzan
            Fernanda</a>
        </div>
    </>
}

export default Footer;