import { Box, Flex, Image, Text } from '@chakra-ui/react';
import React from 'react';
import in_icon from '../assets/images/dashboard/in.png';
import out_icon from '../assets/images/dashboard/out.png';

const HistoryListItem = ({ item }) => {
    return <>
        <Flex alignItems={'center'}>
            <Box border={'1px'} w={['15%', '10%']} borderColor={'gray.100'} p={[2]}>
                <Image src={item.kategori_histories_id === 1 ? in_icon : out_icon} alt='icon' m={'auto'} />
            </Box>
            <Box flex={1} pl={[2]}>
                <Text fontSize={['xs', 'md']}>{item.kegiatan}</Text>
                <Text fontSize={['.6rem', 'sm']} color={'gray.400'}>{item.created_at}</Text>
            </Box>
        </Flex>
    </>
}

export default HistoryListItem;