import { FormControl, FormLabel, Input } from '@chakra-ui/react';
import React from 'react';

const InputText = ({value, name, label, handleChange, isRequired = false}) => {
    return <>
        <FormControl mb={[4]} isRequired={isRequired}>
            <FormLabel fontSize={["xs", "md"]}>
                {label}
            </FormLabel>
            <Input
                size={["md"]}
                name={name}
                value={value}
                onChange={handleChange}
                bg={'white'}
                boxShadow={['lg']}
            />
        </FormControl>
    </>
}

export default InputText;