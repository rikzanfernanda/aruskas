import { Button, FormControl, FormErrorMessage, FormLabel, Input, InputGroup, InputRightElement } from '@chakra-ui/react';
import React, { useState } from 'react';

const InputPassword = ({ value, name, label, handleChange, isRequired }) => {
    const [show, setShow] = useState(false);

    return <>
        <FormControl isInvalid={value.length !== 0 && value.length < 6} mb={[4]} isRequired={isRequired}>
            <FormLabel fontSize={["xs", "md"]}>
                {label}
            </FormLabel>
            <InputGroup>
                <Input
                    size={["md"]}
                    name={name}
                    value={value}
                    type={show ? "text" : "password"}
                    onChange={handleChange}
                    required
                    bg={'white'}
                    boxShadow={['lg']}
                />
                <InputRightElement width="4.5rem">
                    <Button h="1.75rem" size="sm" onClick={() => setShow(!show)}>
                        {show ? "Hide" : "Show"}
                    </Button>
                </InputRightElement>
            </InputGroup>
            <FormErrorMessage>Password harus lebih dari 6 karakter</FormErrorMessage>
        </FormControl>
    </>
}

export default InputPassword;