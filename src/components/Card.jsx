import { Box, Heading } from '@chakra-ui/react';
import React from 'react';

const Card = ({ title, body }) => {
    return <>
        <Box bg={'white'} borderRadius={['lg']} boxShadow={['xl']} p={[3, 6]}>
            <Heading size={['xs', 'md']} mb={[4, 8]}>{title}</Heading>
            {body}
        </Box>
    </>
}

export default Card;