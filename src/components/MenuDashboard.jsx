import { Box, Image, Text } from '@chakra-ui/react';
import React from 'react';
import { Link } from 'react-router-dom';

const MenuDashboard = ({ img, menu, link }) => {
    return <>
        <Box>
            <Link to={link}>
                <Box bg={'white'} borderRadius={['lg']} p={[2, 2, 2, 4]} boxShadow={['xl']} mb={['4px', 1]}>
                    <Image src={img} alt="icon" m={'auto'} />
                </Box>
                <Text fontSize={['.6rem', 'sm']} textAlign={'center'}>{menu}</Text>
            </Link>
        </Box>
    </>
}

export default MenuDashboard;