import { Flex, Heading, Image } from '@chakra-ui/react';
import React from 'react';
import { Link } from 'react-router-dom';
import logo from "../assets/images/logo.png";
import { APP_NAME } from '../config/Config';

const Logo = () => {
    return <>
        <Link to={'/'}>
            <Flex justifyContent={'center'} alignItems={'center'} gap={[2]}>
                <Image src={logo} alt="logo" w={['10%']} />
                <Heading size={['md', 'lg', 'xl']}>{APP_NAME}</Heading>
            </Flex>
        </Link>
    </>
}

export default Logo;