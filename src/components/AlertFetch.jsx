import { Alert, AlertIcon } from '@chakra-ui/react';
import React from 'react';

const AlertFetch = ({ alert }) => {
    return <>
        {
            alert.status && (
                <Alert status={alert.type} fontSize={['xs', 'md']}>
                    <AlertIcon />
                    {alert.message}
                </Alert>
            )
        }
    </>
}

export default AlertFetch;