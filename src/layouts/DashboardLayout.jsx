import { ChevronDownIcon } from '@chakra-ui/icons';
import { Box, Flex, Image, Menu, MenuButton, MenuItem, MenuList, Text } from '@chakra-ui/react';
import jsCookie from 'js-cookie';
import React, { useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import logo from "../assets/images/logo.png";
import { APP_NAME } from '../config/Config';

const DashboardLayout = ({ content }) => {
    let navigate = useNavigate();

    useEffect(() => {
        if (jsCookie.get('_TOKEN') === undefined) navigate('/login', { replace: true });
    }, [navigate]);

    const logout = () => {
        jsCookie.remove('_TOKEN');
        jsCookie.remove('id');
        jsCookie.remove('nama');
        navigate('/login', { replace: true });
    }

    return <>
        <Box bg={'gray.50'} minH={'100vh'}>
            <Flex bg={'white'} alignItems={'center'} justifyContent={'space-between'} py={[2, 3]} px={[2, 20, 40, 60]}>
                <Box>
                    <Link to={'/'}>
                        <Flex alignItems={'center'} gap={[2]} w={'inherit'}>
                            <Image src={logo} alt="logo" h={['16px', '20px']} />
                            <Text fontSize={['sm', '1rem']} w={'auto'} fontWeight={'medium'}>{APP_NAME}</Text>
                        </Flex>
                    </Link>
                </Box>
                <Box>
                    <Menu>
                        <MenuButton fontSize={['sm', '1rem']} fontWeight={'medium'} textAlign={'right'}>
                            {jsCookie.get('nama')}<ChevronDownIcon boxSize={['1.5rem']} />
                        </MenuButton>
                        <MenuList>
                            <MenuItem onClick={logout}>Logout</MenuItem>
                        </MenuList>
                    </Menu>
                </Box>
            </Flex>
            <Box>
                {content}
            </Box>
        </Box>
    </>
}

export default DashboardLayout;