import { Box, Flex, Image } from '@chakra-ui/react';
import React from 'react';
import auth from "../assets/images/auth.svg";
import Logo from '../components/Logo';

const AuthLayout = ({ content }) => {
    return <>
        <Flex>
            <Box w={['100%', '50%']} py={[5]} px={[2, 20, 40]} bg={'gray.50'} h={'100vh'} overflow={'auto'}>
                <Box mb={[4]}>
                    <Logo/>
                </Box>
                {content}
            </Box>
            <Box w={'50%'} display={['none', 'block']} py={20} px={20} bg={'blue.400'} h={'100vh'}>
                <Box boxShadow={'2xl'} bg={'white'} borderRadius={'3xl'} h={'full'}>
                    <Image src={auth} alt="auth image" h={'full'} m={'auto'} />
                </Box>
            </Box>
        </Flex>
    </>
}

export default AuthLayout;