import React from "react";
import { BrowserRouter, Route, Routes as RoutesAs } from "react-router-dom";
import Home from "../pages/Home";
import Register from "../pages/auth/Register";
import AuthLayout from "../layouts/AuthLayout";
import Login from "../pages/auth/Login";
import NoPage from "../pages/NoPage";
import ForgotPassword from "../pages/auth/ForgotPassword";
import ResetPassword from "../pages/auth/ResetPassword";
import DashboardLayout from "../layouts/DashboardLayout";
import Dashboard from "../pages/dashboard/Dashboard";

const Routes = () => {
    return <>
        <BrowserRouter>
            <RoutesAs>
                <Route path='/' element={<Home />} />
                <Route path="/register" element={<AuthLayout content={<Register />} />} />
                <Route path="/login" element={<AuthLayout content={<Login />} />} />
                <Route path="/forgot-password" element={<AuthLayout content={<ForgotPassword />} />} />
                <Route path="/reset-password" element={<AuthLayout content={<ResetPassword />} />} />
                <Route path="/dashboard" element={<DashboardLayout content={<Dashboard />} />} />
                <Route path="*" element={<NoPage />} />
            </RoutesAs>
        </BrowserRouter>
    </>
}

export default Routes;