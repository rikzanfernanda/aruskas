import React, {useState} from "react";
import "../assets/styles/home.scss";
import ScrollAnimation from 'react-animate-on-scroll';
import NavbarHome from "../components/NavbarHome";
import what_is from "../assets/images/what-is.svg";
import aruskas_present from "../assets/images/aruskas_present.svg";
import plan from "../assets/images/plan.svg";
import {Link} from "react-router-dom";
import Footer from "../components/Footer";

const Home = () => {
    const [active, setActive] = useState(true);

    const handleActive = (e) => {
        e.preventDefault();
        return active ? setActive(false) : setActive(true);
    }

    return <>
        <NavbarHome active={active} handleActive={handleActive}/>

        <div id='section1'>
            <div className={'image'}>
                <img src={what_is} alt={'question'}/>
            </div>
            <div className={'content'}>
                <h1>Apa itu Aruskas?</h1>
                <p>
                    Aruskas adalah sebuah aplikasi untuk manajemen keuangan pribadi.
                    Memiliki fitur pencatatan pemasukan dan pengeluaran yang dapat memudahkan
                    Anda dalam manajemen keuangan.
                </p>
            </div>
        </div>

        <div id={'section2'}>
            <ScrollAnimation animateIn='bounceInRight' animateOut='fadeOut'>
                <h1>Kenapa Anda Harus Menggunakan Aruskas?!</h1>
            </ScrollAnimation>
            <ScrollAnimation animateIn='fadeIn' animateOut='fadeOut' delay={500}>
                <p>
                    Kebanyakan orang mengalami kesulitan mengelola keuangan pribadi mereka. Dan sebagian orang
                    lainnya mengelola keuangan pribadi mereka dengan cara manual.
                </p>
                <Link to='/register'>Mulai Kelola Keuangan</Link>
            </ScrollAnimation>
        </div>

        <div id={'section3'}>
            <div className={'content'}>
                <ScrollAnimation animateIn='bounceInLeft' animateOut='fadeOut'>
                    <h1>
                        Aruskas didesain simpel dan mudah digunakan
                    </h1>
                </ScrollAnimation>
            </div>
            <div className={'image'}>
                <ScrollAnimation animateIn='flipInY'>
                    <img src={aruskas_present} alt="img"/>
                </ScrollAnimation>
            </div>
        </div>

        <div id={'section4'}>
            <div className={'image'}>
                <ScrollAnimation animateIn='flipInY'>
                    <img src={plan} alt={'plan'}/>
                </ScrollAnimation>
            </div>
            <div className={'content'}>
                <ScrollAnimation animateIn='bounceInRight' delay={500} animateOut='fadeOut'>
                    <p>
                        Aruskas memiliki fitur <b>plan</b> yang dapat Anda gunakan untuk merencanakan pengeluaran Anda
                        untuk 1 bulan kedepan,
                        sehingga Anda dapat mengetahui perkiraan pengeluaran Anda.
                    </p>
                    <Link to='/register'>Mulai Kelola Keuangan</Link>
                </ScrollAnimation>
            </div>
        </div>

        <Footer/>
    </>
}

export default Home;