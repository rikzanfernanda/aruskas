import {
    Button,
    Flex,
    Text,
} from "@chakra-ui/react";
import React, { useState } from "react";
import InputPassword from "../../components/InputPassword";
import InputText from "../../components/InputText";
import { register } from "../../api/Auth";
import MyAlert from "../../components/MyAlert";
import { Link } from "react-router-dom";
import HeadingAuth from "../../components/HeadingAuth";

const Register = () => {
    const initialInput = {
        nama: "",
        email: "",
        password: "",
        pekerjaan: '',
        alamat: ''
    }
    const [input, setInput] = useState(initialInput);
    const [alert, setAlert] = useState({
        status: false,
        type: "warning",
        message: ""
    });
    const [isLoading, setIsLoading] = useState(false);

    const handleChange = (e) => {
        setInput({
            ...input,
            [e.target.name]: e.target.value,
        });
    };

    const onSubmit = async () => {
        setIsLoading(true);
        try {
            const {data} = await register(input);
            if (data.status === 200) {
                setAlert({
                    status: true,
                    type: "success",
                    message: data.message
                })
                setInput(initialInput)
            } else if (data.status === 422) {
                setAlert({
                    status: true,
                    type: "error",
                    message: data.message.email || data.message.password
                })
            } else {
                setAlert({
                    status: true,
                    type: "warning",
                    message: data.message
                })
            }
        } catch (error) {
            setAlert({
                status: true,
                type: "error",
                message: error.message
            })
        }
        setIsLoading(false);
    }

    return (
        <>
            <HeadingAuth title={'Register'} />

            <MyAlert alert={alert} setAlert={setAlert} />

            <InputText name={'nama'} label={'Nama'} value={input.nama} handleChange={handleChange} isRequired={true} />

            <InputText name={'email'} label={'Email'} value={input.email} handleChange={handleChange} isRequired />

            <InputPassword name={'password'} label={'Password'} value={input.password} handleChange={handleChange} isRequired />

            <InputText name={'pekerjaan'} label={'Pekerjaan'} value={input.pekerjaan} handleChange={handleChange} />

            <InputText name={'alamat'} label={'Alamat'} value={input.alamat} handleChange={handleChange} />

            <Flex gap={4} alignItems={'center'}>
                <Button isLoading={isLoading} onClick={onSubmit} colorScheme='blue' size={['sm', 'md']} isDisabled={!input.nama || !input.email || input.password.length < 6}>Register</Button>
                <Text>Or</Text>
                <Link to={'/login'} className="link-default">Login</Link>
            </Flex>
        </>
    );
};

export default Register;
