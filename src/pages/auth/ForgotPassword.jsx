import { Button, Flex, Text } from '@chakra-ui/react';
import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { forgotPassword } from '../../api/Auth';
import HeadingAuth from '../../components/HeadingAuth';
import InputText from '../../components/InputText';
import MyAlert from '../../components/MyAlert';

const ForgotPassword = () => {
    const [input, setInput] = useState({
        email: ''
    });
    const [isLoading, setIsLoading] = useState(false);
    const [alert, setAlert] = useState({
        status: false,
        type: "warning",
        message: ""
    });

    const handleChange = (e) => {
        setInput({
            ...input,
            [e.target.name]: e.target.value
        });
    }

    const onSubmit = async () => {
        setIsLoading(true);
        
        try {
            const {data} = await forgotPassword(input);
            
            if (data.status === 200) {
                setAlert({
                    status: true,
                    type: 'success',
                    message: "Berhasil request reset password, silakan cek email Anda. Bila tidak ada, cek folder spam."
                })
            } else if (data.status === 422) {
                setAlert({
                    status: true,
                    type: 'warning',
                    message: 'Email tidak ditemukan!'
                })
            } else {
                setAlert({
                    status: true,
                    type: 'warning',
                    message: data.message
                })
            }
        } catch (error) {
            setAlert({
                status: true,
                type: 'error',
                message: error.message
            })
        }

        setIsLoading(false);
    }

    return <>
        <HeadingAuth title={'Lupa Password'} />

        <MyAlert alert={alert} setAlert={setAlert} />

        <InputText name={'email'} value={input.email} handleChange={handleChange} label={'Email Akun Anda'} isRequired={true} />

        <Flex gap={4} alignItems={'center'}>
            <Button isLoading={isLoading} onClick={onSubmit} colorScheme='blue' size={['sm', 'md']} isDisabled={!input.email}>Login</Button>
            <Text>Or</Text>
            <Link to={'/login'} className="link-default">Login</Link>
        </Flex>
    </>
}

export default ForgotPassword;