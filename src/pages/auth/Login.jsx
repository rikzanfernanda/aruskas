import React, { useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import jsCookie from 'js-cookie';
import { Box, Button, Flex, Text } from '@chakra-ui/react';
import MyAlert from '../../components/MyAlert';
import InputText from '../../components/InputText';
import InputPassword from '../../components/InputPassword';
import { login } from '../../api/Auth';
import HeadingAuth from '../../components/HeadingAuth';

const Login = () => {
    let navigate = useNavigate();
    const initialInput = {
        email: "",
        password: ""
    }
    const [input, setInput] = useState(initialInput);
    const [alert, setAlert] = useState({
        status: false,
        type: "warning",
        message: ""
    });
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        if (jsCookie.get('_TOKEN') !== undefined) navigate('/dashboard');
    }, [navigate]);

    const handleChange = (e) => {
        setInput({
            ...input,
            [e.target.name]: e.target.value,
        });
    };

    const onSubmit = async () => {
        setIsLoading(true);
        try {
            const { data } = await login(input);

            if (data.status === 200) {
                let exp = 86400;
                jsCookie.set('_TOKEN', data.token, { expires: exp });
                jsCookie.set('id', data.id, { expires: exp });
                jsCookie.set('nama', data.nama, { expires: exp });
                navigate('/dashboard');
            } else if (data.status === 422) {
                setAlert({
                    status: true,
                    type: 'warning',
                    message: 'Email atau Password salah!'
                })
            } else {
                setAlert({
                    status: true,
                    type: 'warning',
                    message: data.message
                })
            }
        } catch (error) {
            setAlert({
                status: true,
                type: "error",
                message: error.message
            });
        }

        setIsLoading(false);

    }

    return <>
        <HeadingAuth title={'Login'} />

        <MyAlert alert={alert} setAlert={setAlert} />

        <InputText name={'email'} label={'Email'} value={input.email} handleChange={handleChange} isRequired />

        <InputPassword name={'password'} label={'Password'} value={input.password} handleChange={handleChange} isRequired />

        <Box textAlign={'right'} mb={[4]}>
            <Link to="/forgot-password" className='link-default'>Lupa Password</Link>
        </Box>

        <Flex gap={4} alignItems={'center'}>
            <Button isLoading={isLoading} onClick={onSubmit} colorScheme='blue' size={['sm', 'md']} isDisabled={!input.email || input.password.length < 6}>Login</Button>
            <Text>Or</Text>
            <Link to={'/register'} className="link-default">Register</Link>
        </Flex>
    </>
}

export default Login;