import { Button, Flex, Text } from '@chakra-ui/react';
import React, { useState } from 'react';
import { Link, useNavigate, useSearchParams } from 'react-router-dom';
import { resetPassword } from '../../api/Auth';
import HeadingAuth from "../../components/HeadingAuth";
import InputPassword from "../../components/InputPassword";
import MyAlert from "../../components/MyAlert";

const ResetPassword = () => {
    let [searchParams] = useSearchParams();
    let navigate = useNavigate();
    const [input, setInput] = useState({
        password: ''
    });
    const [pass, setPass] = useState('');
    const [alert, setAlert] = useState({
        status: false,
        type: "warning",
        message: ""
    });
    const [isLoading, setIsLoading] = useState(false);

    const handleChange = (e) => {
        setInput({
            ...input,
            [e.target.name]: e.target.value
        });
    }

    const onSubmit = async () => {
        setIsLoading(true);
        if (input.password !== pass) {
            setAlert({
                status: true,
                type: "warning",
                message: "Password tidak sama"
            });
        } else {
            try {
                const { data } = await resetPassword(searchParams.get('token'), input);
                if (data.status === 200) {
                    navigate('/login');
                } else if (data.status === 422) {
                    if (data.message.token) {
                        setAlert({
                            status: true,
                            type: "warning",
                            message: "Token tidak valid"
                        });
                    } else {
                        setAlert({
                            status: true,
                            type: "warning",
                            message: "Panjang password minimal 6 karakter"
                        });
                    }
                } else {
                    setAlert({
                        status: true,
                        type: "warning",
                        message: data.message
                    });
                }
            } catch (error) {
                setAlert({
                    status: true,
                    type: 'error',
                    message: error.message
                })
            }
        }
        setIsLoading(false);
    }

    return <>
        <HeadingAuth title="Reset Password" />

        <MyAlert alert={alert} setAlert={setAlert} />

        <InputPassword name={'password'} value={input.password} handleChange={handleChange} isRequired={true} label={'Password'} />

        <InputPassword name={'password_confirm'} value={pass} handleChange={(e) => setPass(e.target.value)} isRequired={true} label={'Ketik Ulang Password'} />

        <Flex gap={4} alignItems={'center'}>
            <Button isLoading={isLoading} onClick={onSubmit} colorScheme='blue' size={['sm', 'md']} isDisabled={input.password.length < 6 || pass.length < 6}>Ubah Password</Button>
            <Text>Or</Text>
            <Link to={'/login'} className="link-default">Login</Link>
        </Flex>
    </>
}

export default ResetPassword;