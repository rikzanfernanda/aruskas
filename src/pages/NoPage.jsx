import { Box, Button, Image } from '@chakra-ui/react';
import React from 'react';
import { Link } from 'react-router-dom';
import nopage from "../assets/images/nopage.svg";

const NoPage = () => {
    return <>
        <Box textAlign={'center'} h={'100vh'} p={[10]}>
            <Image src={nopage} alt='nopage' h={'60vh'} m={'auto'} />
            <Link to={'/dashboard'}>
                <Button w={['full', 'auto']}>Back</Button>
            </Link>
        </Box>
    </>
}

export default NoPage;