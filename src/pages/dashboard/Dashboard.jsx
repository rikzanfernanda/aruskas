import { Box, SimpleGrid } from '@chakra-ui/react';
import React from 'react';
import bank from "../../assets/images/dashboard/bank.png";
import anggaran from "../../assets/images/dashboard/anggaran.png";
import pemasukan from "../../assets/images/dashboard/pemasukan.png";
import pengeluaran from "../../assets/images/dashboard/pengeluaran.png";
import history from "../../assets/images/dashboard/history.png";
import plan from "../../assets/images/dashboard/plan.png";
import settings from "../../assets/images/dashboard/settings.png";
import MenuDashboard from '../../components/MenuDashboard';
import HistoryList from '../../components/HistoryList';
import Card from '../../components/Card';

const Dashboard = () => {
    return <>
        <Box bg={'blue.400'} color={'white'} px={[2, 20, 40, 60]} pt={[4, 10]} pb={[10, 20]}>
            <SimpleGrid columns={[4, 4, 5, 5, 7]} gap={[6, 10]}>
                <MenuDashboard img={bank} menu={'Bank'} link={'/bank'} />
                <MenuDashboard img={anggaran} menu={'Anggaran'} link={'/anggaran'} />
                <MenuDashboard img={pemasukan} menu={'Pemasukan'} link={'/pemasukan'} />
                <MenuDashboard img={pengeluaran} menu={'Pengeluaran'} link={'/pengeluaran'} />
                <MenuDashboard img={history} menu={'History'} link={'/history'} />
                <MenuDashboard img={plan} menu={'Plan'} link={'/plan'} />
                <MenuDashboard img={settings} menu={'Settings'} link={'/settings'} />
            </SimpleGrid>
        </Box>
        <Box px={[2, 20, 40, 60]} py={[4]}>
            <Card title={'Histori Terkini'} body={<HistoryList limit={5} />} />
        </Box>
    </>
}

export default Dashboard;