import axios from 'axios';
import {BASE_URL} from '../config/Config';

const REGISTER = `${BASE_URL}/api/register`;

const LOGIN = `${BASE_URL}/api/login`;

const FORGOT_PASSWORD = `${BASE_URL}/api/forgot-password`;

const RESET_PASSWORD = `${BASE_URL}/api/reset-password`;

export const register = (value) => {
    return axios.post(REGISTER, value)
}

export const login = (value) => {
    return axios.post(LOGIN, value)
}

export const forgotPassword = (value) => {
    return axios.post(FORGOT_PASSWORD, value)
}

export const resetPassword = (token, value) => {
    return axios.post(RESET_PASSWORD + "?token=" + token, value)
}

export {
    REGISTER,
    LOGIN,
    FORGOT_PASSWORD,
    RESET_PASSWORD
}