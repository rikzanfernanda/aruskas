import axios from "axios";
import jsCookie from "js-cookie";
import { BASE_URL } from "../config/Config";

const HISTORIES = BASE_URL + "/api/histories";

export const getHistories = (year, limit) => {
    let config = {
        headers: {
            Authorization: "bearer " + jsCookie.get("_TOKEN"),
        },
    };
    
    if (year) {
        return axios.get(HISTORIES + "/" + year, config);
    }

    if (limit) {
        return axios.get(HISTORIES + "?limit=" + limit, config);
    }

    return axios.get(HISTORIES, config);
};

export { HISTORIES };
